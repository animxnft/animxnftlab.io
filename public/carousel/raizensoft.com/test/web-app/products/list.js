
var wpProducts = [
    {
      "price":29,
      "name":"3D Photo Cloud",
      "thumbnail":"cloudwp.jpg",
      "link":"https://preview.codecanyon.net/item/3d-photo-cloud-wordpress-media-plugin/full_screen_preview/30262581"
    },
    {
      "price":29,
      "name":"3D Photo Queue",
      "thumbnail":"queuewp.jpg",
      "link":"https://preview.codecanyon.net/item/3d-photo-queue-wordpress-plugin/full_screen_preview/30311212"
    },
    {
      "price":29,
      "name":"3D Photo Tile",
      "thumbnail":"tilewp.jpg",
      "link":"https://preview.codecanyon.net/item/3d-photo-tile-wordpress-plugin/full_screen_preview/30238651"
    },
    {
      "price":29,
      "name":"3D Photo Row",
      "thumbnail":"rowwp.jpg",
      "link":"https://preview.codecanyon.net/item/3d-photo-row-wordpress-media-plugin/full_screen_preview/30179344"
    },
    {
      "price":29,
      "name":"3D Photo Wall",
      "thumbnail":"wallwp.jpg",
      "link":"https://preview.codecanyon.net/item/3d-photo-wall-wordpress-media-plugin/full_screen_preview/30399892"
    },
    {
      "price":29,
      "name":"3D Carousel Stack",
      "thumbnail":"carouselwp.jpg",
      "link":"https://preview.codecanyon.net/item/3d-carousel-stack-gallery-wordpress-plugin/full_screen_preview/31103398"
    }
  ];

var jsProducts = [
    {
      "price":22,
      "name":"3D Photo Cloud",
      "thumbnail":"cloud.jpg",
      "link":"https://preview.codecanyon.net/item/3d-photo-cloud/full_screen_preview/26565583"
    },
    {
      "price":22,
      "name":"3D Photo Wheel",
      "thumbnail":"wheel.jpg",
      "link":"https://preview.codecanyon.net/item/3d-photo-wheel/full_screen_preview/26566194"
    },
    {
      "price":22,
      "name":"3D Photo Book",
      "thumbnail":"book.jpg",
      "link":"https://preview.codecanyon.net/item/3d-photo-wheel/full_screen_preview/26566194"
    },
    {
      "price":22,
      "name":"3D Carousel Stack Gallery",
      "thumbnail":"carousel.jpg",
      "link":"https://preview.codecanyon.net/item/3d-carousel-stack-gallery/full_screen_preview/26584747"
    },
    {
      "price":22,
      "name":"3D Photo Box",
      "thumbnail":"box.jpg",
      "link":"https://preview.codecanyon.net/item/3d-photo-box/full_screen_preview/26590923"
    },
    {
      "price":22,
      "name":"3D Photo Queue",
      "thumbnail":"queue.jpg",
      "link":"https://preview.codecanyon.net/item/3d-photo-queue/full_screen_preview/26574758"
    },
    {
      "price":22,
      "name":"3D Photo Row",
      "thumbnail":"row.jpg",
      "link":"https://preview.codecanyon.net/item/3d-photo-row/full_screen_preview/26654646"
    },
    {
      "price":22,
      "name":"3D Spherical Gallery",
      "thumbnail":"sphere.jpg",
      "link":"https://preview.codecanyon.net/item/3d-spherical-gallery-advanced-media-gallery/full_screen_preview/28449898"
    },
    {
      "price":22,
      "name":"3D Cubic Gallery",
      "thumbnail":"cubic.jpg",
      "link":"https://preview.codecanyon.net/item/3d-cubic-gallery-advanced-media-gallery/full_screen_preview/29068799"
    },
    {
      "price":22,
      "name":"3D Photo Wall",
      "thumbnail":"wall.jpg",
      "link":"https://preview.codecanyon.net/item/3d-photo-wall-advanced-media-gallery/full_screen_preview/29237739"
    },
    {
      "price":22,
      "name":"3D Spiral Gallery",
      "thumbnail":"spiral.jpg",
      "link":"https://preview.codecanyon.net/item/3d-spiral-gallery-advanced-media-gallery/full_screen_preview/29415456"
    },
    {
      "price":22,
      "name":"3D Pyramid Gallery",
      "thumbnail":"pyramid.jpg",
      "link":"https://preview.codecanyon.net/item/3d-pyramid-gallery-advanced-media-gallery/full_screen_preview/29474109"
    },
    {
      "price":22,
      "name":"3D Mosaic Gallery",
      "thumbnail":"mosaic.jpg",
      "link":"https://preview.codecanyon.net/item/3d-mosaic-gallery-advanced-media-gallery/full_screen_preview/29600976"
    },
    {
      "price":22,
      "name":"3D Photo Scroll",
      "thumbnail":"scroll.jpg",
      "link":"https://preview.codecanyon.net/item/3d-photo-scroll-advanced-media-gallery/full_screen_preview/29664966"
    },
    {
      "price":22,
      "name":"3D Photo Tile",
      "thumbnail":"tile.jpg",
      "link":"https://preview.codecanyon.net/item/3d-photo-tile-advanced-media-gallery/full_screen_preview/29895536"
    },
    {
      "price":22,
      "name":"3D Carousel Flow",
      "thumbnail":"carouselflow.jpg",
      "link":"https://preview.codecanyon.net/item/3d-carousel-flow-advanced-media-gallery/full_screen_preview/29940392"
    },
    {
      "price":22,
      "name":"3D Photo Room",
      "thumbnail":"room.jpg",
      "link":"https://preview.codecanyon.net/item/3d-photo-room-advanced-media-gallery/full_screen_preview/30457430"
    },
    {
      "price":22,
      "name":"3D Photo Card",
      "thumbnail":"card.jpg",
      "link":"https://preview.codecanyon.net/item/3d-photo-card-advanced-media-gallery/full_screen_preview/30929161"
    },
    {
      "price":22,
      "name":"3D Flipping Grid",
      "thumbnail":"flip.jpg",
      "link":"https://preview.codecanyon.net/item/3d-flipping-grid-gallery-advanced-media-gallery/full_screen_preview/31053823"
    }
  ];

function shufflearray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}
shufflearray(wpProducts);
shufflearray(jsProducts);

listWpProduct({
  "products":wpProducts.splice(0, 4)
});

listProduct({
  "products":jsProducts.splice(0, 16)
});

