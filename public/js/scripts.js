(function($) {
	$(document).ready(function() {
		"use strict";
	
		$(".project-box").hover(function () {
			// change hovered div background to yellow:
			 $(".works").css("background-color", $(this).data('bg'));
			// loop through the rest of the divs with the same class:
			$(".works").not(this).each(function(){
				// change their background color according to their 'data-color' attribute:
				$(this).css("background-color", $(this).data('bg'));
			});
		   // set hover out method:
		}, function(){
			// change each 'box' background color to default:
			$(".works").css("background-color", '');
		});	
			

		// PRELOADER
		loader()
		function loader(_success) {
			var obj = document.querySelector('.preloader'),
			inner = document.querySelector('.inner .percentage'),
			page = document.querySelector('body');
			obj.classList.remove('page-loaded');
			page.classList.add('page-loaded');
			var w = 0,
				t = setInterval(function() {
					w = w + 1;
					inner.textContent = w;
					if (w === 100){
						obj.classList.add('page-loaded');
						page.classList.remove('page-loaded');
						clearInterval(t);
						w = 0;
						if (_success){
							return _success();
						}
					}
				}, 20);
		}

		/* MAGNET CURSOR*/
        var cerchio = document.querySelectorAll('.magnet-link');
        cerchio.forEach(function(elem) {
            $(document).on('mousemove touch', function(e) {
                magnetize(elem, e);
            });
        })

        function magnetize(el, e) {
            var mX = e.pageX,
                mY = e.pageY;
            const item = $(el);

            const customDist = item.data('dist') * 20 || 80;
            const centerX = item.offset().left + (item.width() / 2);
            const centerY = item.offset().top + (item.height() / 2);

            var deltaX = Math.floor((centerX - mX)) * -0.35;
            var deltaY = Math.floor((centerY - mY)) * -0.35;

            var distance = calculateDistance(item, mX, mY);

            if (distance < customDist) {
                TweenMax.to(item, 0.5, {
                    y: deltaY,
                    x: deltaX,
                    scale: 1
                });
                item.addClass('magnet');
            } else {
                TweenMax.to(item, 0.6, {
                    y: 0,
                    x: 0,
                    scale: 1
                });
                item.removeClass('magnet');
            }
        }

        function calculateDistance(elem, mouseX, mouseY) {
            return Math.floor(Math.sqrt(Math.pow(mouseX - (elem.offset().left + (elem.width() / 2)), 2) + Math.pow(mouseY - (elem.offset().top + (elem.height() / 2)), 2)));
        }

        function lerp(a, b, n) {
            return (1 - n) * a + n * b
        }

        // Inizio Cursor
        class Cursor {
            constructor() {
                this.bind()
                //seleziono la classe del cursore
                this.cursor = document.querySelector('.js-cursor')

                this.mouseCurrent = {
                    x: 0,
                    y: 0
                }

                this.mouseLast = {
                    x: this.mouseCurrent.x,
                    y: this.mouseCurrent.y
                }

                this.rAF = undefined
            }

            bind() {
                ['getMousePosition', 'run'].forEach((fn) => this[fn] = this[fn].bind(this))
            }

            getMousePosition(e) {
                this.mouseCurrent = {
                    x: e.clientX,
                    y: e.clientY
                }
            }

            run() {
                this.mouseLast.x = lerp(this.mouseLast.x, this.mouseCurrent.x, 0.2)
                this.mouseLast.y = lerp(this.mouseLast.y, this.mouseCurrent.y, 0.2)

                this.mouseLast.x = Math.floor(this.mouseLast.x * 100) / 100
                this.mouseLast.y = Math.floor(this.mouseLast.y * 100) / 100

                this.cursor.style.transform = `translate3d(${this.mouseLast.x}px, ${this.mouseLast.y}px, 0)`

                this.rAF = requestAnimationFrame(this.run)
            }

            requestAnimationFrame() {
                this.rAF = requestAnimationFrame(this.run)
            }

            addEvents() {
                window.addEventListener('mousemove', this.getMousePosition, false)
            }

            on() {
                this.addEvents()

                this.requestAnimationFrame()
            }

            init() {
                this.on()
            }
        }

        const my_cursor = new Cursor()

        my_cursor.init();

        $('img, a, .sandwich, .equalizer, button, .swiper-pagination-bullet, .swiper-button-prev, .swiper-button-next, .main-nav').hover(function() {
            $('.cursor').toggleClass('light');
        });
		
		// ICON CONTENT BLOCK
			$('.icon-content-block .content-block').mouseenter(function() {
				$('.selected').removeClass('selected').addClass('icon-content-block .content-block');
				$(this).removeClass('icon-content-block .content-block').addClass('selected');
			});
		
		

		// SPLITTING
			Splitting();
		
		
		
		// DATA BACKGROUND IMAGE
			// var pageSection = $(".swiper-slide");
			// pageSection.each(function(indx){
			// 	if ($(this).attr("data-background")){
			// 		$(this).css("background-image", "url(" + $(this).data("background") + ")");
			// 	}
			// });

		//DATA BACKGROUND IMAGE
		var pageSection = $("*");
		pageSection.each(function(indx) {
			if ($(this).attr("data-background")) {
				$(this).css("background-image", "url(" + $(this).data("background") + ")");
			}
		});

		

		// var pageSection = $(".bg-image");
		// pageSection.each(function(indx){
		// 	if ($(this).attr("data-background")){
		// 		$(this).css("background-image", "url(" + $(this).data("background") + ")");
		// 	}
		// });


		
			
		
		
		
		// TREE MENU
		$('.site-navigation .inner ul li i').click(function () {
	  	$(this).parent().children('.site-navigation .inner ul li ul').slideToggle(300);
        return true;
	  	});

		
		
		// HAMBURGER MENU
		$('.hamburger').on('click', function(e) {
			if ($(".site-navigation").hasClass("active")) {
				$("body").toggleClass("overflow");
				$(".site-navigation").removeClass("active");
				$(".site-navigation").css("transition-delay", "0.5s");
				$(".site-navigation .layer").css("transition-delay", "0.3s");
				$(".site-navigation .inner").css("transition-delay", "0s");
			} else
			{
				$(".site-navigation").addClass('active');
				$("body").toggleClass("overflow");
				$(".site-navigation.active").css("transition-delay", "0s");
				$(".site-navigation.active .layer").css("transition-delay", "0.2s");
				$(".site-navigation.active .inner").css("transition-delay", "0.7s");
			}
			$(this).toggleClass("is-opened-navi");
		});
		
		
		// FOLLOW US
		$('.follow-us').on('click', function(e) {
			if ($(".social-media").hasClass("active")) {
				$("body").toggleClass("overflow");
				$(".social-media").removeClass("active");
				$(".social-media").css("transition-delay", "0.5s");
				$(".social-media .layer").css("transition-delay", "0.3s");
				$(".social-media .inner").css("transition-delay", "0s");
			} else
			{
				$(".social-media").addClass('active');
				$("body").toggleClass("overflow");
				$(".social-media.active").css("transition-delay", "0s");
				$(".social-media.active .layer").css("transition-delay", "0.2s");
				$(".social-media.active .inner").css("transition-delay", "0.7s");
			}
			
		});
		
		
		
		// ALL CASES
		$('.all-cases-link b').on('click', function(e) {
			if ($(".all-cases").hasClass("active")) {
				$("body").toggleClass("overflow");
				$(".all-cases").removeClass("active");
				$(".all-cases").css("transition-delay", "0.5s");
				$(".all-cases .layer").css("transition-delay", "0.3s");
				$(".all-cases .inner").css("transition-delay", "0s");
			} else
			{
				$(".all-cases").addClass('active');
				$("body").toggleClass("overflow");
				$(".all-cases.active").css("transition-delay", "0s");
				$(".all-cases.active .layer").css("transition-delay", "0.2s");
				$(".all-cases.active .inner").css("transition-delay", "0.7s");
			}
			
		});
		

		
		// CONTACT FORM INPUT LABEL
			function checkForInput(element) {
			  const $label = $(element).siblings('span');
			  if ($(element).val().length > 0) {
				$label.addClass('label-up');
			  } else {
				$label.removeClass('label-up');
			  }
			}

			// The lines below are executed on page load
			$('input, textarea').each(function() {
			  checkForInput(this);
			});

			// The lines below (inside) are executed on change & keyup
			$('input, textarea').on('change keyup', function() {
			  checkForInput(this);  
			});
	
	
	
	

	
		
		
		
		// EQUALIZER TOGGLE
			var source = "audio/audio.mp3";
			var audio = new Audio(); // use the constructor in JavaScript, just easier that way
			audio.addEventListener("load", function() {
			  audio.play();
			}, true);
			audio.src = source;
			audio.autoplay = true;
			audio.loop = true;
			audio.volume = 0.2;

			$('.equalizer').click();		
			var playing = true;		
			$('.equalizer').click(function() {
				if (playing == false) {
			  audio.play();
					playing = true;

				} else {
					audio.pause();
					playing = false;
				}
			});
	
	
	
	// EQUALIZER
			function randomBetween(range) {
				var min = range[0],
					max = range[1];
				if (min < 0) {
					return min + Math.random() * (Math.abs(min)+max);
				}else {
					return min + Math.random() * max;
				}
			}

			$.fn.equalizerAnimation = function(speed, barsHeight){
				var $equalizer = $(this);	
				setInterval(function(){
					$equalizer.find('span').each(function(i){
					  $(this).css({ height:randomBetween(barsHeight[i])+'px' });
					});
				},speed);
				$equalizer.on('click',function(){
					$equalizer.toggleClass('paused');
				});
			};

			var barsHeight = [
			  [2, 13],
			  [5, 22],
			  [17, 8],
			  [4, 18],
			  [11, 3]
			];
			$('.equalizer').equalizerAnimation(180, barsHeight);
		
		
		
		// PAGE TRANSITION
		$('body a').on('click', function(e) {

			if (typeof $( this ).data('fancybox') == 'undefined') {
			e.preventDefault(); 
			var url = this.getAttribute("href"); 
			if( url.indexOf('#') != -1 ) {
			var hash = url.substring(url.indexOf('#'));

			if( $('body ' + hash ).length != 0 ){
			$('.transition-overlay').removeClass("active");
			$(".hamburger").toggleClass("open");
			$("body").toggleClass("overflow");
			$(".navigation-menu").removeClass("active");
			$(".navigation-menu .inner ul").css("transition-delay", "0s");
			$(".navigation-menu .inner blockquote").css("transition-delay", "0s");
			$(".navigation-menu .bg-layers span").css("transition-delay", "0.3s");

			$('html, body').animate({
			scrollTop: $(hash).offset().top
			}, 1000);

			}
			}
			else {
			$('.transition-overlay').toggleClass("active");
			setTimeout(function(){
			window.location = url;
			},600); 

			}
			}
			});
		
		
		
		
		});
	// END JQUERY	
	
	

	// PRELOADER
	var width = 100,
	perfData = window.performance.timing, // The PerformanceTiming interface represents timing-related performance information for the given page.
	EstimatedTime = -(perfData.loadEventEnd - perfData.navigationStart),
	time = parseInt((EstimatedTime/1000)%60, 10)*100;

	$(".loadbar").animate({
		width: width + "%"
		}, time);

	function animateValue(id, start, end, duration) {

	var range = end - start,
		current = start,
		increment = end > start? 1 : -1,
		stepTime = Math.abs(Math.floor(duration / range)),
		obj = $(id);

	var timer = setInterval(function() {
		current += increment;
		$(obj).text(current + "%");
		if (current == end) {
			clearInterval(timer);
		}
	}, stepTime);
	}

	// SCROLL BG COLOR
	$(window).scroll(function() {
		var $window = $(window),
			$body = $('body'),
			$panel = $('section, footer, header');

		var scroll = $window.scrollTop() + ($window.height() / 3);

		$panel.each(function () {
		  var $this = $(this);
		  if ($this.position().top <= scroll && $this.position().top + $this.height() > scroll) {

			$(this).removeClass(function (index, css) {
			  return (css.match (/(^|\s)background-color-\S+/g) || []).join(' ');
			});

			$(this).css("background-color", $(this).data('bg'));
			
		  }
		});    

	  }).scroll();

	setTimeout(function(){
		$("body").addClass("page-loaded");
	}, time);
	
    // SLIDER
		var mainslider = new Swiper('.gallery-top', {
			spaceBetween: 0,
			autoplay: {
			delay: 9500,
			disableOnInteraction: false,
			},
			navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
			},
			pagination: {
			el: '.swiper-pagination',
			type: 'progressbar',
			},
			loop: true,
			loopedSlides: 3,
			thumbs: {
			swiper: sliderthumbs
		  }
		});

	// TYPEWRITER
	$("#typewriter").typewriter({
		prefix : "",
		text : ["KHALID", "ECOM GROWTH HACKER", "THE ECOM DRAGON"],
		typeDelay : 100,
		waitingTime : 1500,
		blinkSpeed : 800
	});
	
	// SLIDER
	var carouselslider = new Swiper('.carousel-slider', {
		spaceBetween: 0,
		slidesPerView: 3,
		centeredSlides: true,
		autoplay: {
		delay: 9500,
		disableOnInteraction: false,
		},
		navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
		},
		pagination: {
		el: '.swiper-pagination',
		type: 'progressbar',
		},
		loop: true,
		breakpoints: {
		1024: {
		  slidesPerView: 3
		},
		768: {
		  slidesPerView: 2
		},
		640: {
		  slidesPerView: 1
		},
		320: {
		  slidesPerView: 1
		}
	  }
	});

	
	// SLIDER THUMBS
		var sliderthumbs = new Swiper('.gallery-thumbs', {
			spaceBetween: 10,
			centeredSlides: true,
			slidesPerView: 3,
			touchRatio: 0.2,
			slideToClickedSlide: true,
			loop: true,
			loopedSlides: 3,
			breakpoints: {
			1024: {
			  slidesPerView: 3
			},
			768: {
			  slidesPerView: 1
			},
			640: {
			  slidesPerView: 1
			},
			320: {
			  slidesPerView: 1
			}
		  }
			});

		if ($(".gallery-top")[0]){
			mainslider.controller.control = sliderthumbs;
			sliderthumbs.controller.control = mainslider;
			} 
		else {

			}
	
	
	
	
	// OFFICE SLIDER
			new Swiper('.office-slider', {
		  slidesPerView: '1',
		  spaceBetween: 0,
				centeredSlides: true,
				loop: true,
		  pagination: {
			el: '.swiper-pagination',
			clickable: true,
		  },
		});
	
	
	
	
	
	
	
	
	
	// TESTIMONIALS
			new Swiper('.testimonials-slider', {
		  slidesPerView: '1',
		  spaceBetween: 0,
				centeredSlides: true,
				loop: true,
		  pagination: {
			el: '.swiper-pagination',
			clickable: true,
		  },
		});




		// COUNTER
			 $(document).scroll(function(){
				$('.odometer').each( function () {
					var parent_section_postion = $(this).closest('section').position();
					var parent_section_top = parent_section_postion.top;
					if ($(document).scrollTop() > parent_section_top - 300) {
						if ($(this).data('status') == 'yes') {
							$(this).html( $(this).data('count') );
							$(this).data('status', 'no');
						}
					}
				});
			});

	
		// WOW ANIMATION 
			wow = new WOW(
			{
				animateClass: 'animated',
				offset:       200
			}
			);
			wow.init();

	
		// PRELOADER
		$(window).load(function(){
			$("body").addClass("page-loaded1");	
		});

		
	
})(jQuery);	